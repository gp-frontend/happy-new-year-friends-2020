let svgcss = require('gulp-svg-css');
    iconsPath = {
    'src': 'src/icons/*.svg',
    'srcTemplate': 'src/website/utils/sprite/template/sprite_template.pcss',
    'srcIconsBase': 'src/icons/base/*.svg',
    'destScss': './src/website/utils/sprite/',
    'destSprite': './build/images/sprite',
  };

module.exports = () => {

  // generate svg sprite
  $.gulp.task('svg:sprite', () => {

    return $.gulp.src(iconsPath.src)

      .pipe($.plugins.newer(iconsPath.destSprite))

      .pipe($.plugins.svgmin({

        js2svg: {
          pretty: true
        }
      }))

      .pipe($.plugins.cheerio({

        run: function ($) {
          $('[fill]').removeAttr('fill');
          $('[stroke]').removeAttr('stroke');
          $('[style]').removeAttr('style');
        },

        parserOptions: {xmlMode: true}
      }))

      .pipe($.plugins.replace('&gt;;', '>'))

      .pipe($.plugins.svgSprite({

        mode: {
          symbol: {
            sprite: '../sprite.svg',

            render: {

              pcss: {
                dest:'../sprite.pcss',
                template: iconsPath.srcTemplate
              }
            }
          }
        }
      }))

      .pipe( $.plugins.if('*.pcss', $.gulp.dest(iconsPath.destScss), $.gulp.dest(iconsPath.destSprite)) )

      .pipe($.plugins.notify(' I make icons sprite!'));
  });

  // generate svg to base 64 code
  $.gulp.task('svg:base', () => {

    return $.gulp.src(iconsPath.srcIconsBase)

      .pipe($.plugins.svgmin())

      .pipe(svgcss({
        fileName: 'spite-base',
        addSize: true
      }))


      .pipe($.gulp.dest(iconsPath.destScss))
  });
};
