// TODO: REMOVE THIS TASK

let gcmq = require('gulp-group-css-media-queries'),
    stylesPath = {
      'src': 'src/styles/*.scss',
      'dest': 'build/css'
    };

module.exports = () => {

  // DEV TASK
  $.gulp.task('styles:dev', () => {

    return $.gulp.src(stylesPath.src)

      .pipe($.plugins.plumber({

        errorHandler: function(err) {

         $.plugins.notify.onError({
            title: "Error in SCSS file",
            message: "<%= error.message %>"
          })(err);
        }
      }))

      .pipe($.plugins.sourcemaps.init())

      .pipe($.plugins.sass())

      .pipe($.plugins.autoprefixer({
        browsers: ['last 2 versions']
      }))

      .pipe(gcmq())

      .pipe($.plugins.sourcemaps.write())

      .pipe($.plugins.rename({
          extname: '.min.css'
        }),
      )

      .pipe($.gulp.dest(stylesPath.dest))

      .pipe($.browserSync.stream());
  });

  // PROD TASK
  $.gulp.task('styles:prod', () => {

    return $.gulp.src(stylesPath.src)

      .pipe($.plugins.sass({
        errLogToConsole: false,
        onError: function(err) {
          return $.plugins.notify().write(err);
        }
      }))

      .pipe($.plugins.autoprefixer({
        browsers: ['last 2 versions']
      }))

      .pipe(gcmq())

      // [TODO] Include CSSCOMB

      .pipe($.gulp.dest(stylesPath.dest))
      .pipe($.plugins.debug({
        title:'SASS compile done !'}
      ))

      .pipe($.plugins.csso())

      .pipe($.plugins.rename({
          extname: '.min.css'
        }),
      )

      .pipe($.gulp.dest(stylesPath.dest))

      .pipe($.plugins.debug({
        title:'Styles minify done !'}
      ))
  });
};
